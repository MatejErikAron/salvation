# REDEMPTION

Keď sa rozhodnete zavrhnúť mier a dobré vzťahy, tak sa rozhodnete pre utrpenie a bolesť. Toto rozhodnutie je len vaše, avšak mier je možný mať len medzi všetkými stranami a zvoliť si, že bude trpieť jeden, znamená zvoliť si, že budete trpieť obaja.

Vyberajte a snáď si vyberiete správne. Len nezabúdajte, že nekonať je taktiež len ďalšia voľba - voľba, ktorá nevedie k mieru.